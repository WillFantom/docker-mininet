#!/bin/bash

service openvswitch-switch start
ovs-vsctl set-manager ptcp:6640

echo 
echo "Cleaning MN"
echo

sudo mn -c

echo 
echo "Running Topology: " $TOPO
echo

if [ -z "${MN_FLAGS}" ]; then 
    sudo python3 $TOPO
else
    sudo mn --custom $TOPO --topo $TOPO_NAME $MN_FLAGS
fi

service openvswitch-switch stop
