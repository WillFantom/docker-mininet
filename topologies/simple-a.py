from mininet.topo import Topo 

# Keep this class name the same in your topologies!!
class AdvNet( Topo ):

    def __init__( self ):
        Topo.__init__( self )

        # Add hosts and switches
        host1 = self.addHost( 'h1' )
        host2 = self.addHost( 'h2' )
        host3 = self.addHost( 'h3' )
        switch1 = self.addSwitch( 's1' )

        # Add links
        self.addLink( host1, switch1 )
        self.addLink( host2, switch1 )
        self.addLink( host3, switch1 )


# Keep this part the same for use in the CLI
topos = { 'topology': ( lambda: AdvNet() ) }