# Docker Mininet

A Dockerized version of Mininet

## For use in SCC365 (Lancaster University Comp Sci)

This allows you to run mininet without having to use the VM provided. This can be useful if you want to use your own device.
You should **not** have to build the image, instead, you can just run using the version pushed to Docker Hub.

Whilst you are currently in a terminal session that is in the same directory as your topology file (e.g. `topo.py` with a topo named `topology`), you can run:

```bash
docker run --rm -it --privileged --network "host" -v $(pwd):/topologies -e TOPO=topo.py -e TOPO_NAME=topology willfantom/mininet:latest
```

If you are trying to run a topo that does not use MN to run (uses `python topo.py` rather than `sudo mn ...`), use the following:
```bash
docker run --rm -it --privileged --network "host" -v $(pwd):/topologies -e TOPO=topo.py -e MN_FLAGS="" willfantom/mininet:latest
```

## Using the Mininet REST API

This is also used to build/distribute the [Mininet REST API project](https://gitlab.com/WillFantom/mininet). To run, you can use the same command as above for the SCC365 usage. You should however use the `rest` tag. Be aware that this will also require port 8080. 

If you wish to use the REST API with a topology that uses MN to launch, be sure to add it to the `MN_FLAGS`:
```bash
docker run --rm -it --privileged --network "host" -v $(pwd):/topologies -e TOPO=topo.py -e TOPO_NAME=topology -e MN_FLAGS="--mac --switch ovsk --rest" willfantom/mininet:rest
```

## Building

This builds mininet from the master src branch by default. However, the args in the dockerfile allow you to change branch or repo.

```bash
docker build --rm --compress -t mininet:dev .
```
