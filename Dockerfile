FROM debian:stretch-slim

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -yq
RUN apt-get install -yq \
    arping \
    git \
    hping3 \
    iputils-ping \
    net-tools \
    sudo \
    traceroute

ARG MN_BRANCH=master
ARG MN_REPO=https://github.com/mininet/mininet
RUN git clone -b ${MN_BRANCH} ${MN_REPO} /root/mininet
RUN sh -c "/root/mininet/util/install.sh -fnv"

RUN apt-get clean

COPY entrypoint.sh /root/entrypoint.sh
RUN chmod +x /root/entrypoint.sh

ARG TOPO_BASE=/topologies
COPY topologies ${TOPO_BASE}
ENV TOPO simple-a.py
ENV TOPO_NAME topology

ENV MN_FLAGS="--controller remote --mac --switch ovsk"

WORKDIR ${TOPO_BASE}
ENTRYPOINT [ "sh", "-c", "/root/entrypoint.sh"]